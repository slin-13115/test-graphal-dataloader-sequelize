import {
  sequelize,
  Op,
  UsersModel,
  PostsModel,
  FollowersModel
} from "./server/models";

import { dBSetUp } from "./server/setup";
import { _ } from "underscore";

// UsersModel.findAll({
//   include: [
//     {
//       model: PostsModel
//     }
//   ],
//   raw: true
// }).then(results => {
//   //   const formattedRes = _.map(results, function(result) {
//   //     return result.dataValues;
//   //   });
//   //   console.log(JSON.stringify(formattedRes));
//   console.log(JSON.stringify(results));
// });

const findByIdDestroyAndCreateUser = async ({ id, name }) => {
  await UsersModel.findById(id).then(res => {
    if (res !== null) {
      res.destroy();
    }
  });
  return await UsersModel.create({ id: id, name: name }).then(res => {
    return res.toJSON();
  });
};

const IIFE0 = async () => {
  await dBSetUp();
  //   console.log(JSON.stringify());
  await findByIdDestroyAndCreateUser({ id: 4, name: "No one" });
  const user4 = await UsersModel.findById(4);
  const pMIdMax = await PostsModel.max("id");
  console.log(`max of PostsModel primary key: ${pMIdMax}`);
  await PostsModel.bulkCreate([
    { id: 4, content: "Say nothing!" },
    { id: 5, content: "Lala!" }
  ]);
  //   const postsToBeSet = await Promise.all([
  //     PostsModel.findById(4),
  //     PostsModel.findById(5)
  //   ]);
  const postsToBeSet = await PostsModel.findAll({
    where: {
      id: {
        [Op.between]: [pMIdMax + 1, pMIdMax + 2]
      }
    }
  });
  const setPostsResult = await user4.setPosts(postsToBeSet);
  //   console.log(`setPostsResult: ${JSON.stringify(setPostsResult)}`);
  const toLog = await UsersModel.findById(4, {
    include: [
      {
        model: PostsModel
      }
    ]
    // raw: true
  });
  console.log(toLog.toJSON());
};

const IIFE1 = async () => {
  await dBSetUp();
  //   const result = await UsersModel.find({
  //     where: {
  //       id: 7
  //     }
  //   });
  //   if (!result) console.log(result);
  //   const findAllRes = await UsersModel.findAll({
  //     include: [
  //       {
  //         model: PostsModel
  //       }
  //     ]
  //     //   { raw: true }
  //   }).then(res => {
  //     const newRes = _.map(res, function(res) {
  //       return res.toJSON();
  //     });
  //     return newRes;
  //   });
  //   console.log(JSON.stringify(findAllRes));
  //   console.log(await findByIdDestroyAndCreateUser({ id: 4, name: "No one" }));
  const createRes = await UsersModel.create(
    {
      id: 4,
      name: "No one",
      posts: [{ id: 4, content: "Say nothing!" }, { id: 5, content: "Lala!" }]
    },
    { include: PostsModel }
  ).then(res => {
    return res.toJSON();
  });
  console.log(JSON.stringify(createRes));
};
// IIFE1();

import { createConnection } from "./server/setup";
import Sequelize from "sequelize";

const IIFE2 = async () => {
  const newSQL = createConnection();
  const temp = newSQL.define(
    "temps",
    {
      field0: {
        type: Sequelize.DECIMAL(65, 18)
      },
      field1: {
        type: Sequelize.DECIMAL(65, 18)
      }
    },
    { timestamps: false }
  );

  await newSQL.sync({ force: true });
  // const queryStr = `INSERT INTO temps VALUE (1, CAST(22 AS DECIMAL(65, 18)) / 7)`;
  // const result = await sequelize.query(queryStr, {
  //   raw: true,
  //   type: sequelize.QueryTypes.INSERT
  // });
  // // console.log(result[0][`ROUND((14620 / 9432456) / (24250 / 9432456), 6)`]);
  // console.log(result);
  // const result = await temp.create({
  //   field0: Sequelize.literal(`CAST(${"22.22222222"} AS DECIMAL(65, 18)) / 3`),
  //   field1: Sequelize.literal(`(CAST(${22} AS DECIMAL(65, 18))/7) * 3`)
  // });
  // console.log(result.toJSON());

  // const val1 = `12345.428571428571428571`,
  //   opStr = `-${val1} + 12345.428571428571428571`;
  // const result1 = await sequelize.query(`SELECT ${opStr}`, {
  //   raw: true,
  //   type: sequelize.QueryTypes.SELECT
  // });

  // console.log(result1[0][opStr]);
  newSQL
    .transaction(async function(t) {
      await temp.create({
        field0: Sequelize.literal(
          `CAST(${"22.22222222"} AS DECIMAL(65, 18)) / 3`
        ),
        field1: Sequelize.literal(`(CAST(${22} AS DECIMAL(65, 18))/7) * 3`)
      });
      // throw new Error("Interrupting transaction!!!");
      // await temp.create({
      //   field0: "I'm a string!!!",
      //   field2: "No such field"
      // });
      await temp.create({
        field0: Sequelize.literal(`CAST(${"3"} AS DECIMAL(65, 18)) / 3`),
        field1: Sequelize.literal(`(CAST(${15} AS DECIMAL(65, 18))/7) * 3`)
      });
    })
    .catch(console.error);
};
IIFE2();
