import { Op, UsersModel, PostsModel, FollowersModel } from "./models";
import { EXPECTED_OPTIONS_KEY } from "dataloader-sequelize";
import { _ } from "underscore";

export const resolvers = {
  Query: {
    users(root, args, context) {
      return UsersModel.findAll().then(users => {
        context.prime(users); // priming the cache for later queries against UsersModel
        // in a nested graphql query
        return users;
      });
    }
  },
  Mutation: {
    removeFollowerFromPost(root, args, context) {
      return FollowersModel.destroy({
        where: {
          postId: args.postId,
          followerId: args.followerId
        }
      });
    }
  },
  User: {
    posts(parent, args, context) {
      return PostsModel.findAll({
        where: {
          authorId: parent.id
        }
      }).then(posts => {
        context.prime(posts);
        return posts;
      });
    }
  },
  Post: {
    followers(parent, args, context) {
      return FollowersModel.findAll({
        // attributes: ["followerId"],
        // not filtering fields because of priming the cache later
        where: {
          postId: parent.id
        }
      }).then(followers => {
        context.prime(followers);
        const followerIds = _.pluck(followers, "followerId");
        return _.map(followerIds, function(followerId) {
          // return a list of promises
          // by thie point, the results from earlier findAll have been cached
          // so, use {[EXPECTED_OPTIONS_KEY]: context} to access the cache
          return UsersModel.findById(followerId, {
            [EXPECTED_OPTIONS_KEY]: context
          });
        });
      });
    }
  }
};
