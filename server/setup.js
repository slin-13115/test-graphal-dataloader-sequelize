import { makeExecutableSchema } from "graphql-tools";
import { typeDefs } from "./schema";
import { resolvers } from "./resolvers";

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

import {
  sequelize,
  Op,
  UsersModel,
  PostsModel,
  FollowersModel
} from "../server/models";

export const dBSetUp = async function() {
  await sequelize.sync({ force: true }).catch(error => {
    console.error(`Error in syncing/creating tables to DB: ${error}`);
  });
  await UsersModel.bulkCreate([
    { id: 1, name: "bruce" },
    { id: 2, name: "steve" },
    { id: 3, name: "tony" }
  ]);
  await PostsModel.bulkCreate([
    { id: 1, content: "Avengers, Assemble!!!", authorId: 2 },
    { id: 2, content: "I'm Batman!!!", authorId: 1 },
    { id: 3, content: "Iron Man?", authorId: 3 }
  ]);
  await FollowersModel.bulkCreate([
    { id: 1, postId: 1, followerId: 3 },
    { id: 2, postId: 2, followerId: 1 },
    { id: 3, postId: 2, followerId: 3 },
    { id: 4, postId: 2, followerId: 2 }
  ]);
};

import Sequelize from "sequelize";
import cls from "continuation-local-storage";
const namespace = cls.createNamespace("test-dl-sql");
Sequelize.useCLS(namespace);

export function createConnection() {
  const connection = new Sequelize("testDLSQL", "test", null, {
    host: "localhost",
    dialect: "mysql",
    operatorsAliases: Op,
    logging: false
  });
  return connection;
}
