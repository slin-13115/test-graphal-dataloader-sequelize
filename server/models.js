import Sequelize from "sequelize";
import dataloaderSequelize from "dataloader-sequelize";

const Op = Sequelize.Op;
const sequelize = new Sequelize("testDLSQL", "test", null, {
  host: "localhost",
  dialect: "mysql",
  operatorsAliases: Op,
  logging: false
});

const UsersModel = sequelize.define(
  "users",
  {
    name: { type: Sequelize.STRING }
  },
  { timestamps: false }
);

const PostsModel = sequelize.define(
  "posts",
  {
    content: { type: Sequelize.STRING },
    authorId: {
      type: Sequelize.INTEGER,
      references: {
        model: "users",
        key: "id"
      }
    }
  },
  { timestamps: false }
);

const FollowersModel = sequelize.define(
  "followers",
  {
    postId: {
      type: Sequelize.INTEGER,
      references: {
        model: "posts",
        key: "id"
      }
    },
    followerId: {
      type: Sequelize.INTEGER,
      references: {
        model: "users",
        key: "id"
      }
    }
  },
  { timestamps: false }
);

UsersModel.hasMany(PostsModel, { foreignKey: "authorId" });
PostsModel.hasMany(FollowersModel);
FollowersModel.belongsTo(UsersModel, { foreignKey: "followerId" });

dataloaderSequelize(sequelize);

// sequelize.sync().catch(error => {
//   console.error(`Error in syncing/creating tables to DB: ${error}`);
// });

export { sequelize, Op, UsersModel, PostsModel, FollowersModel };
