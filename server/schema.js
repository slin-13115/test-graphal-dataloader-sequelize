export const typeDefs = `
type Post {
    id: Int
    content: String
    followers: [User]
}

type User {
    id: Int
    name: String!
    posts: [Post]
}

type Query {
    users: [User]
}

type Mutation {
    removeFollowerFromPost(postId: Int!, followerId: Int!): Int
    # updatePostContent(postId: Int!, content: String): Post
}
`;
