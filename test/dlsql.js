import assert from "assert";
import sinon from "sinon";

import { createContext } from "dataloader-sequelize";
import {
  sequelize,
  UsersModel,
  PostsModel,
  FollowersModel
} from "../server/models";
import { graphql } from "graphql";

import { schema, dBSetUp } from "../server/setup";

describe("test batching and caching of dataloader-sequelize", function() {
  beforeEach(function() {
    this.sandbox = sinon.createSandbox();
  });
  afterEach(function() {
    this.sandbox.restore();
  });

  describe("spying on the UsersModel", function() {
    beforeEach(async function() {
      this.spy = this.sandbox.spy(UsersModel, "findAll");
      dBSetUp();
      this.context = createContext(sequelize);
    });

    it(`nested query against UsersModel`, async function() {
      const getAllUsersQuery = `
            query getAllUsers {
                users {
                    id
                    name
                    posts {
                        content
                        followers {
                            name
                        }
                    }
                }
            }
            `;
      await graphql(schema, getAllUsersQuery, null, this.context, null).then(
        res => {
          console.log(`graphql query result: ${JSON.stringify(res)}`);
        }
      );

      console.log(`UsersModel findAll's call count: ${this.spy.callCount}`);
      assert(this.spy.calledOnce);
    });

    it(`query -> mutation -> query`, async function() {
      await FollowersModel.create({ id: 5, postId: 1, followerId: 2 });
      const queryMutationQuery = `
              query getAllUsers {
                  users {
                      id
                      name
                      posts {
                          content
                          followers {
                              name
                          }
                      }
                  }
              }
  
              mutation RemoveFollowerFromPost($postId: Int!, $followerId: Int!) {
                  removeFollowerFromPost(postId: $postId, followerId: $followerId)
              }
  
              query getAllUsersAgain {
                  users {
                      id
                      name
                      posts {
                          content
                          followers {
                              name
                          }
                      }
                  }
              }
              `;
      await graphql(schema, queryMutationQuery, null, this.context, {
        postId: 1,
        followerId: 2
      }).then(res => {
        console.log(`graphql query result: ${JSON.stringify(res)}`);
      });

      console.log(`UsersModel findAll's call count: ${this.spy.callCount}`);
      //   assert(this.spy.calledOnce);
    });
  });
});
